loading_table = "Dating113"

-- load res
path = "resources/src/lua/table/secondary/"
date = require(path .. "en/" .. loading_table)
dating_rule = require(path .. "DatingRule")
dating_process = require(path .. "DatingProcess")
string_en = require(path .. "en/String")

ending_cat = {
  [1] = "HAPPY END",
  [2] = "TRUE END",
  [3] = "NORMAL END",
  [4] = "BAD END",
  [5] = "HIDDEN END"
}

function print_diag(id)
  print(date[id].text)
end

function print_ending_synopsis(key)
  for i, n in pairs(ending_cat) do
    print(ending_cat[i])
    for k, v in pairs(dating_process[key]["end" .. tostring(i) .. "Synopsis"]) do
      print("Route End: " .. string_en[v].text)
    end
    print()
  end
end

-- STARTS HERE
for k, v in pairs(dating_rule) do
  if v.callTableName == loading_table then
    print("Ending for scripts " .. k)
    print_ending_synopsis(k)
    print()
  end
end
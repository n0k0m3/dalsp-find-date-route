return {
  ["Dating101"] = {
    [2101001] = {
      datingTitle = "Bakery",
      start_node_id = 1002100001,
      [2501220] = {
        endType = 3,
        endSynopsis = "Bittersweet",
      },
      [2502122] = {
        endType = 4,
        endSynopsis = "Failure is the mother of success",
      },
      [2502465] = {
        endType = 5,
        endSynopsis = "Here they are!",
      },
      [2505113] = {
        endType = 2,
        endSynopsis = "Some says sakura falls at five centimeters per second.",
      },
      [2601221] = {
        endType = 2,
        endSynopsis = "That kind of things, I can teach you all!",
      },
      [2601313] = {
        endType = 3,
        endSynopsis = "I like everything about you",
      },
      [2602107] = {
        endType = 4,
        endSynopsis = "I lost this time",
      },
      [2602215] = {
        endType = 1,
        endSynopsis = "My heart will be the crown of your glory",
      },
      [2603210] = {
        endType = 4,
        endSynopsis = "Death Seeker",
      },
      [2604306] = {
        endType = 3,
        endSynopsis = "World peace",
      },
    },
    [2101002] = {
      datingTitle = "Doll Shop",
      start_node_id = 1009100001,
      [9503316] = {
        endType = 5,
        endSynopsis = "Great Treasure",
      },
      [9602117] = {
        endType = 4,
        endSynopsis = "I lost my way of life",
      },
      [9603123] = {
        endType = 3,
        endSynopsis = "Sleeping Detective",
      },
      [9702128] = {
        endType = 1,
        endSynopsis = "I am the envoy from the sea",
      },
      [9703118] = {
        endType = 2,
        endSynopsis = "Virgin Mary",
      },
      [9703211] = {
        endType = 4,
        endSynopsis = "Lawless Land",
      },
      [9801128] = {
        endType = 2,
        endSynopsis = "See the light again",
      },
      [9801217] = {
        endType = 3,
        endSynopsis = "A wise man submits to food.",
      },
    },
    [2101004] = {
      datingTitle = "Supermarket",
      start_node_id = 316100001,
      [316502121] = {
        endType = 5,
        endSynopsis = "I have a pet!",
      },
      [316601116] = {
        endType = 3,
        endSynopsis = "It's my duty to protect Tohka",
      },
      [316601208] = {
        endType = 3,
        endSynopsis = "Protect Tohka's smile",
      },
      [316601523] = {
        endType = 3,
        endSynopsis = "Unique Taste",
      },
      [316601818] = {
        endType = 4,
        endSynopsis = "Failed to buy ingredient",
      },
      [316601937] = {
        endType = 1,
        endSynopsis = "Cook Tohka",
      },
      [316602118] = {
        endType = 2,
        endSynopsis = "Love for You",
      },
      [316602507] = {
        endType = 1,
        endSynopsis = "Foodie Tohka",
      },
      [316602620] = {
        endType = 5,
        endSynopsis = "New bakery idea found",
      },
    },
  },
  ["Dating102"] = {
    [2102001] = {
      datingTitle = "Bakery",
      start_node_id = 30100001,
      [30502122] = {
        endType = 2,
        endSynopsis = "Turns out AST is not what I think it is",
      },
      [30502227] = {
        endType = 5,
        endSynopsis = "Origami, Don't...",
      },
      [30601119] = {
        endType = 3,
        endSynopsis = "Fight for the fate of human!",
      },
      [30601225] = {
        endType = 1,
        endSynopsis = "We are in this together",
      },
      [30601323] = {
        endType = 4,
        endSynopsis = "Disappear, everything!",
      },
      [30602217] = {
        endType = 4,
        endSynopsis = "Let Me Explain!",
      },
      [30701120] = {
        endType = 3,
        endSynopsis = "Maybe that's my destiny",
      },
      [30702114] = {
        endType = 2,
        endSynopsis = "The right way to feed",
      },
      [30703218] = {
        endType = 3,
        endSynopsis = "I'd take it even it is poisonous",
      },
    },
    [2102004] = {
      datingTitle = "Flower Store (Night)",
      start_node_id = 120100001,
      [120501225] = {
        endType = 3,
        endSynopsis = "Sincere Thanks",
      },
      [120502110] = {
        endType = 2,
        endSynopsis = "Sunny after Rain",
      },
      [120505110] = {
        endType = 4,
        endSynopsis = "Leaving Reflection",
      },
      [120603212] = {
        endType = 2,
        endSynopsis = "Zero Distance Temperature",
      },
      [120604112] = {
        endType = 3,
        endSynopsis = "Overflowing Emotion",
      },
      [120605110] = {
        endType = 2,
        endSynopsis = "Flower and Girl",
      },
      [120605206] = {
        endType = 3,
        endSynopsis = "Hard to Decide",
      },
      [120701135] = {
        endType = 1,
        endSynopsis = "Gift",
      },
      [120702111] = {
        endType = 4,
        endSynopsis = "Subtle Fragility",
      },
      [120703208] = {
        endType = 2,
        endSynopsis = "Witness of Bonds",
      },
      [120704211] = {
        endType = 3,
        endSynopsis = "Running in the Rain",
      },
      [120704306] = {
        endType = 5,
        endSynopsis = "Night Butterfly",
      },
    },
    [2102005] = {
      datingTitle = "Beach Resort",
      start_node_id = 121100001,
      [121501125] = {
        endType = 3,
        endSynopsis = "Change Pose Next Time",
      },
      [121501346] = {
        endType = 1,
        endSynopsis = "Right Way to Apply Lotion",
      },
      [121501613] = {
        endType = 3,
        endSynopsis = "Make Your Wish Come True",
      },
      [121501719] = {
        endType = 2,
        endSynopsis = "Swimsuit of Scheme",
      },
      [121502115] = {
        endType = 1,
        endSynopsis = "Hold Her Tight",
      },
      [121502420] = {
        endType = 2,
        endSynopsis = "Your Mission",
      },
      [121502619] = {
        endType = 1,
        endSynopsis = "The Lovers",
      },
      [121503222] = {
        endType = 5,
        endSynopsis = "Super Dinner",
      },
      [121503313] = {
        endType = 5,
        endSynopsis = "Legend of Shells",
      },
      [121601308] = {
        endType = 4,
        endSynopsis = "Leave for Jealousy",
      },
      [121601410] = {
        endType = 3,
        endSynopsis = "Odd Way to Apply Lotion?",
      },
    },
    [2102006] = {
      datingTitle = "Bath House",
      start_node_id = 123100001,
      [123401331] = {
        endType = 3,
        endSynopsis = "Just Sleeping",
      },
      [123402122] = {
        endType = 4,
        endSynopsis = "Grudge in Heart",
      },
      [123501234] = {
        endType = 2,
        endSynopsis = "Table Tennis Match",
      },
      [123501430] = {
        endType = 3,
        endSynopsis = "Escape",
      },
      [123501550] = {
        endType = 5,
        endSynopsis = "Pure White Moonlight",
      },
      [123501610] = {
        endType = 3,
        endSynopsis = "Sorry, Origami",
      },
      [123501719] = {
        endType = 1,
        endSynopsis = "Another Side of Origami",
      },
      [123801120] = {
        endType = 2,
        endSynopsis = "Hot Spring Egg",
      },
    },
  },
  ["Dating103"] = {
    [2103002] = {
      datingTitle = "Doll Shop",
      start_node_id = 25100001,
      [25601126] = {
        endType = 1,
        endSynopsis = "Return with Full Hands",
      },
      [25601318] = {
        endType = 4,
        endSynopsis = "Frozen Doll Shop",
      },
      [25604115] = {
        endType = 1,
        endSynopsis = "Oasis of Mind",
      },
      [25604214] = {
        endType = 3,
        endSynopsis = "Doll's New Master",
      },
      [25701126] = {
        endType = 3,
        endSynopsis = "Crane Machine Hero",
      },
      [25701229] = {
        endType = 2,
        endSynopsis = "Crane Machine Master",
      },
      [25702121] = {
        endType = 3,
        endSynopsis = "Unexpected Doll",
      },
      [25702215] = {
        endType = 5,
        endSynopsis = "Giant Yoshinon",
      },
      [25703310] = {
        endType = 4,
        endSynopsis = "Mad Yoshinon",
      },
    },
    [2103003] = {
      datingTitle = "Park",
      start_node_id = 225100001,
      [225501370] = {
        endType = 1,
        endSynopsis = "Warrior Yoshino",
      },
      [225501486] = {
        endType = 3,
        endSynopsis = "Realistic Acting",
      },
      [225501579] = {
        endType = 2,
        endSynopsis = "Curtain Call",
      },
      [225501721] = {
        endType = 4,
        endSynopsis = "Screwed Up",
      },
      [225501848] = {
        endType = 3,
        endSynopsis = "Demon and Servant",
      },
      [225501922] = {
        endType = 4,
        endSynopsis = "Yoshino is Gone",
      },
      [225601117] = {
        endType = 5,
        endSynopsis = "Yoshino and Squirrel",
      },
      [225601319] = {
        endType = 3,
        endSynopsis = "Silence",
      },
      [225701316] = {
        endType = 2,
        endSynopsis = "Courage",
      },
    },
    [2103005] = {
      datingTitle = "Park at Night",
      start_node_id = 308100001,
      [308501126] = {
        endType = 3,
        endSynopsis = "Ara, my cover is blown",
      },
      [308501332] = {
        endType = 5,
        endSynopsis = "Get Together Again",
      },
      [308502511] = {
        endType = 3,
        endSynopsis = "What a good parent",
      },
      [308502723] = {
        endType = 2,
        endSynopsis = "Love Guru",
      },
      [308503016] = {
        endType = 3,
        endSynopsis = "Spotted! Playboy",
      },
      [308503218] = {
        endType = 3,
        endSynopsis = "That's too casual",
      },
      [308503314] = {
        endType = 2,
        endSynopsis = "Girl's Best Friend",
      },
      [308503416] = {
        endType = 2,
        endSynopsis = "Inversed Ending",
      },
      [308601114] = {
        endType = 1,
        endSynopsis = "You are a perfect lover",
      },
      [308601216] = {
        endType = 4,
        endSynopsis = "Get Separated Instead",
      },
      [308601319] = {
        endType = 1,
        endSynopsis = "It's Painful",
      },
    },
  },
  ["Dating104"] = {
    [2104001] = {
      datingTitle = "Bakery",
      start_node_id = 1028100001,
      [28701113] = {
        endType = 3,
        endSynopsis = "Feed the stray dog with ice cream cake",
      },
      [28702215] = {
        endType = 2,
        endSynopsis = "You are special to me",
      },
      [28703220] = {
        endType = 3,
        endSynopsis = "Do you like the smell of shirt",
      },
      [28704113] = {
        endType = 1,
        endSynopsis = "I want to eat your cake",
      },
      [28704320] = {
        endType = 2,
        endSynopsis = "Eat you mildly",
      },
      [28705119] = {
        endType = 4,
        endSynopsis = "He belongs with me",
      },
      [28705221] = {
        endType = 4,
        endSynopsis = "Instinct to Escape",
      },
      [28801124] = {
        endType = 3,
        endSynopsis = "Exclusive! Just to Meet You",
      },
      [28801230] = {
        endType = 5,
        endSynopsis = "Kurumi's Top Secret",
      },
    },
    [2104002] = {
      datingTitle = "Doll Shop",
      start_node_id = 1029100001,
      [29702121] = {
        endType = 4,
        endSynopsis = "Daydream",
      },
      [29703112] = {
        endType = 3,
        endSynopsis = "You must be the new guy",
      },
      [29703206] = {
        endType = 3,
        endSynopsis = "Crisis resolved! Crisis resolved!",
      },
      [29705126] = {
        endType = 2,
        endSynopsis = "Kurumi, do you know a certain steward?",
      },
      [29705227] = {
        endType = 1,
        endSynopsis = "Bell Cat",
      },
      [29705320] = {
        endType = 2,
        endSynopsis = "There is only one truth!",
      },
      [29706225] = {
        endType = 4,
        endSynopsis = "Is my sister really that powerful?",
      },
      [29801127] = {
        endType = 3,
        endSynopsis = "I was freaked out",
      },
      [29803122] = {
        endType = 5,
        endSynopsis = "Failed Attempt to Look Cool",
      },
    },
    [2104004] = {
      datingTitle = "Shrine (Night)",
      start_node_id = 119100001,
      [119402513] = {
        endType = 2,
        endSynopsis = "Wanna Date You Again",
      },
      [119501117] = {
        endType = 1,
        endSynopsis = "Sweet Date",
      },
      [119501217] = {
        endType = 3,
        endSynopsis = "You Are so Sweet",
      },
      [119501516] = {
        endType = 3,
        endSynopsis = "Brush Past the Treasures",
      },
      [119501722] = {
        endType = 5,
        endSynopsis = "Thanks for Finding It for Me",
      },
      [119502114] = {
        endType = 4,
        endSynopsis = "Busy for Nothing",
      },
      [119502214] = {
        endType = 2,
        endSynopsis = "Mysterious Date Started",
      },
      [119502422] = {
        endType = 3,
        endSynopsis = "Come Again Tomorrow",
      },
      [119502709] = {
        endType = 2,
        endSynopsis = "Fruit of Hard Work",
      },
      [119502816] = {
        endType = 2,
        endSynopsis = "Buried Coin",
      },
      [119503510] = {
        endType = 3,
        endSynopsis = "Get Teased Instead",
      },
      [119503611] = {
        endType = 4,
        endSynopsis = "Scary Kurumi",
      },
    },
  },
  ["Dating105"] = {
    [2105003] = {
      datingTitle = "School (Night)",
      start_node_id = 122100001,
      [122601112] = {
        endType = 1,
        endSynopsis = "Sister's Swoop",
      },
      [122601231] = {
        endType = 3,
        endSynopsis = "Scorpion Hairpin",
      },
      [122601429] = {
        endType = 3,
        endSynopsis = "All for Nothing",
      },
      [122601922] = {
        endType = 4,
        endSynopsis = "Flee away",
      },
      [122701173] = {
        endType = 5,
        endSynopsis = "Book about Spirits",
      },
      [122701820] = {
        endType = 5,
        endSynopsis = "Strange and Funny Book",
      },
      [122801129] = {
        endType = 4,
        endSynopsis = "Security Ruined the Quest",
      },
      [122801312] = {
        endType = 4,
        endSynopsis = "Forever Siblings",
      },
      [122801627] = {
        endType = 2,
        endSynopsis = "Spell of Pumpkin Carriage",
      },
      [122801743] = {
        endType = 1,
        endSynopsis = "Sleep in arms",
      },
      [122801869] = {
        endType = 3,
        endSynopsis = "Ghost Busted",
      },
    },
    [2105004] = {
      datingTitle = "Bakery",
      start_node_id = 65100001,
      [65603116] = {
        endType = 2,
        endSynopsis = "Change the world for my sister",
      },
      [65701121] = {
        endType = 1,
        endSynopsis = "Kotori's Swoop",
      },
      [65701216] = {
        endType = 3,
        endSynopsis = "Usage of Milk",
      },
      [65703118] = {
        endType = 3,
        endSynopsis = "Cutest when You Smile",
      },
      [65705114] = {
        endType = 2,
        endSynopsis = "Capable Sister",
      },
      [65705224] = {
        endType = 3,
        endSynopsis = "Do Whatever She Wants",
      },
      [65707127] = {
        endType = 4,
        endSynopsis = "Hard to choose",
      },
      [65708209] = {
        endType = 4,
        endSynopsis = "Quarrel",
      },
      [65801121] = {
        endType = 5,
        endSynopsis = "How can my little sister be so cute",
      },
    },
  },
  ["Dating110"] = {
    [2110001] = {
      datingTitle = "Clothing Store",
      start_node_id = 488100001,
      [488501622] = {
        endType = 3,
        endSynopsis = "Escape in a hurry",
      },
      [488501822] = {
        endType = 3,
        endSynopsis = "Close Distance",
      },
      [488501925] = {
        endType = 5,
        endSynopsis = "Date for Four",
      },
      [488502336] = {
        endType = 1,
        endSynopsis = "Fitting Room for Two",
      },
      [488601120] = {
        endType = 1,
        endSynopsis = "Diva Miku",
      },
      [488601224] = {
        endType = 2,
        endSynopsis = "Sing for You",
      },
      [488601529] = {
        endType = 5,
        endSynopsis = "Lovers' clothes?",
      },
    },
  },
  ["Dating112"] = {
    [2112001] = {
      datingTitle = "Accessory Shop",
      start_node_id = 313100001,
      [313401417] = {
        endType = 4,
        endSynopsis = "Disqualified",
      },
      [313601725] = {
        endType = 3,
        endSynopsis = "Bell Rings",
      },
      [313601837] = {
        endType = 5,
        endSynopsis = "Girl and Pigeon",
      },
      [313602129] = {
        endType = 2,
        endSynopsis = "Nice to meet you",
      },
      [313602209] = {
        endType = 4,
        endSynopsis = "Teasing",
      },
      [313701121] = {
        endType = 2,
        endSynopsis = "Will we meet again?",
      },
      [313701222] = {
        endType = 3,
        endSynopsis = "How to Qualify",
      },
      [313701448] = {
        endType = 1,
        endSynopsis = "Transparent Mood",
      },
    },
    [2112002] = {
      datingTitle = "Amusement Park",
      start_node_id = 314100001,
      [314502108] = {
        endType = 2,
        endSynopsis = "I'm by your side.",
      },
      [314502228] = {
        endType = 3,
        endSynopsis = "Heart Pounds",
      },
      [314502413] = {
        endType = 3,
        endSynopsis = "Plan the next date",
      },
      [314502522] = {
        endType = 1,
        endSynopsis = "Touch dolphin",
      },
      [314503226] = {
        endType = 5,
        endSynopsis = "Hot balloon in Sunset",
      },
      [314601112] = {
        endType = 4,
        endSynopsis = "Observer's Mission",
      },
      [314601230] = {
        endType = 5,
        endSynopsis = "Souvenir",
      },
      [314601323] = {
        endType = 1,
        endSynopsis = "Get closer",
      },
      [314601510] = {
        endType = 4,
        endSynopsis = "Unpleasant date",
      },
      [314601614] = {
        endType = 1,
        endSynopsis = "Wish for that day to come",
      },
      [314601810] = {
        endType = 3,
        endSynopsis = "Play around",
      },
    },
  },
  ["Dating113"] = {
    [2113001] = {
      datingTitle = "Flower Shop",
      start_node_id = 460100001,
      [460401833] = {
        endType = 5,
        endSynopsis = "Cotton Wreath",
      },
      [460501319] = {
        endType = 2,
        endSynopsis = "Creative Blue Rose",
      },
      [460501425] = {
        endType = 1,
        endSynopsis = "Flower of Hope",
      },
      [460501732] = {
        endType = 4,
        endSynopsis = "Natsumi's Reflection",
      },
      [460502133] = {
        endType = 4,
        endSynopsis = "Sudden Bad Mood",
      },
      [460502330] = {
        endType = 1,
        endSynopsis = "Seed",
      },
      [460601309] = {
        endType = 3,
        endSynopsis = "Expected Love",
      },
      [460601418] = {
        endType = 3,
        endSynopsis = "Noob Florist",
      },
      [460701103] = {
        endType = 4,
        endSynopsis = "Incomplete Date",
      },
      [460701208] = {
        endType = 1,
        endSynopsis = "Everyone Likes Natsumi",
      },
    },
    [2113002] = {
      datingTitle = "Park",
      start_node_id = 461100001,
      [461401514] = {
        endType = 4,
        endSynopsis = "Role Model",
      },
      [461501415] = {
        endType = 3,
        endSynopsis = "Keep Working Hard",
      },
      [461601328] = {
        endType = 2,
        endSynopsis = "Power of Confidence",
      },
      [461601414] = {
        endType = 3,
        endSynopsis = "How to Get Along",
      },
      [461601518] = {
        endType = 5,
        endSynopsis = "No Confidence",
      },
      [461601810] = {
        endType = 4,
        endSynopsis = "Runaway",
      },
      [461701146] = {
        endType = 1,
        endSynopsis = "Maple Dusk",
      },
      [461701230] = {
        endType = 2,
        endSynopsis = "Hide and Seek",
      },
      [461701422] = {
        endType = 3,
        endSynopsis = "Swing for Two",
      },
    },
  },
}